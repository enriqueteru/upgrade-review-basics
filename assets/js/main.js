window.onload = () => {
const movies = [
        {title: 'Madaraspar', duration: 192, categories: ['comedia', 'aventura']},
        {title: 'Spiderpan', duration: 122, categories: ['aventura', 'acción']},
        {title: 'Solo en Whatsapp', duration: 223, categories: ['comedia', 'thriller']},
        {title: 'El gato con guantes', duration: 111, categories: ['comedia', 'aventura', 'animación']},
    ];
    
    const categories = (arr) => {
    //Creamos un nuevo array
    const arrayCategoria = [];
    //Iteramos las categoráis dentro del nuevo array 
    for (movie of arr){
        for (item of movie.categories){
            arrayCategoria.push(item);};
    };
    //creamos un SET de valores únicos 
    let noDuplicates = new Set(arrayCategoria);
    //Devolvemos el SET
    return noDuplicates
    };


//categories(movies);


const users = [
    {name: 'Manolo el del bombo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 50},
            rain: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'Mortadelo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 30},
            shower: {format: 'ogg', volume: 55},
            train: {format: 'mp3', volume: 60},
        }
    },
    {name: 'Super Lopez',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 67},
            shower: {format: 'mp3', volume: 50},
            train: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'El culebra',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 100},
            wind: {format: 'ogg', volume: 35},
            firecamp: {format: 'mp3', volume: 60},
        }
    },
];

let avgValuesWaves = () => {

let volumes = [];

let favoriteSong = users.map(item => item.favoritesSounds);
let second = favoriteSong.map(a => a);
let tree = second.map(i => i.waves);
let media = tree.forEach(e => volumes.push(e.volume));
let sumVolumes = volumes.reduce((a,b)=> {
return (a + b) }, 0);

let avg = sumVolumes / volumes.length;
console.log('La media de waves es: ' + parseInt(avg));
return sumVolumes / volumes.length;
};


avgValuesWaves();



// Iteración #4: Métodos findArrayIndex
let arrRandom = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote']

const findArrayIndex = (arr, text) => {
let textParam = text;
for (item in arr) {
if (arr[item] === textParam) {
console.log(`la posición en el array de la palabra "${textParam}" es igual a ${item}`)
}}};

findArrayIndex(arrRandom, 'Mosquito')



//Iteración #5: Función rollDice

let tiraDado = (number) => {

let carasDelDado = number;
let tirada = parseInt(Math.random()*( number - 1) + 2 );
console.log(`el dado tiene ${carasDelDado} caras, tu número es: ${tirada}`)
return tirada;


}
tiraDado(6);



//Iteración #6: Swap

let arr = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño']

const swap = (arr, index1, index2)=>{

Array.prototype.swapItems = function(a, b){
    this[a] = this.splice(b, 1, this[a])[0];
    return this;
};

console.log('Antes de swap: ' + arr)
return console.log('Despues de swap: ' + arr.swapItems(index1, index2));

};

swap(arr, 0,3);

}